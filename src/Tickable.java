/**
 * Interface for classes that provide the tick method
 */
public interface Tickable {
    /**
     * Process one tick of gametime
     *
     * @param dt Time since the last tick in seconds
     */
    void tick(float dt);
}
