import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by jamie on 4/22/17.
 */
public class MazeParser {
    // What the fucking fuck is fucking java fucking fuck fuck fuck
    private static int y;
    private static float x;

    public static List<MazeWall> parse(Path path) {
        List<MazeWall> walls = new ArrayList<>();
        try {
            y = -3;
            Files.lines(path).forEachOrdered(line -> {
                y++;

                if (y >= 0) {

                    x = -2f;
                    line.chars().forEachOrdered(c -> {
                        x += 0.5;

                        if (x >= 0) {
                            if (x % 1 == 0) {
                                if (c == '|') {
                                    // Itsa (vertical) wall, Mario!

                                    walls.add(new MazeWall((int)x, y-1, true));
                                }

                                // Char can be _ or ' ', it's a stylistic choice on map maker's part
                            } else if (x % 1 == 0.5) {
                                assert c != '|';

                                if (c == '_') {
                                    walls.add(new MazeWall((int) Math.floor(x), y, false));
                                }
                            } else {
                                assert false;
                            }
                        }
                    });
                }
            });
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        return walls;
    }
}
