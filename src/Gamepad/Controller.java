package Gamepad;

/**
 * Guaranteed to have the buttons:
 * A, B, X, Y, START, BACK, L_BUTTON, R_BUTTON, L_SHOULDER, R_SHOULDER, HOME
 *
 * Guaranteed to have the analog values:
 * L_STICK_X, L_STICK_Y, L_TRIGGER, R_STICK_X, R_STICK_Y, R_TRIGGER, DPAD_X, DPAD_Y
 */
public class Controller extends Gamepad {
	public Controller(String filePath) {
		super(filePath);
		setButtonName("A", (short) 304);
		setButtonName("B", (short) 305);
		setButtonName("X", (short) 307);
		setButtonName("Y", (short) 308);
		setButtonName("START", (short) 315);
		setButtonName("BACK", (short) 314);
		setButtonName("L_BUTTON", (short) 317);
		setButtonName("R_BUTTON", (short) 318);
		setButtonName("L_SHOULDER", (short) 310);
		setButtonName("R_SHOULDER", (short) 311);
		setButtonName("HOME", (short) 316);

		setAnalogName("L_STICK_X", 0, -32767, 32767);
		setAnalogName("L_STICK_Y", 1, -32767, 32767);
		setAnalogName("R_STICK_X", 3, -32767, 32767);
		setAnalogName("R_STICK_Y", 4, -32767, 32767);
		setAnalogName("L_TRIGGER", 2, 0, 255);
		setAnalogName("R_TRIGGER", 5, 0, 255);
		setAnalogName("DPAD_X", 16, -1, 1);
		setAnalogName("DPAD_Y", 17, -1, 1);
	}
}