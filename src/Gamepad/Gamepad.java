package Gamepad;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.*;
import java.util.function.Consumer;

public class Gamepad {

    private Map<Short, Boolean> buttonPresses = Collections.synchronizedMap(new HashMap<>());
    private Map<Short, Integer> analogValues = Collections.synchronizedMap(new HashMap<>());

    private Map<String, Short> buttonNames = Collections.synchronizedMap(new HashMap<>());
	private Map<Short, String> buttonIDs = Collections.synchronizedMap(new HashMap<>());
    private Map<String, Short> analogNames = Collections.synchronizedMap(new HashMap<>());
	private Map<Short, String> analogIDs = Collections.synchronizedMap(new HashMap<>());
	private Map<Short, Integer> analogMin = Collections.synchronizedMap(new HashMap<>());
	private Map<Short, Integer> analogMax = Collections.synchronizedMap(new HashMap<>());


    private BufferedInputStream stream;

    private List<Consumer<String>> listeners = new ArrayList<>();

    public Gamepad(String filePath) {
        try {
            stream = new BufferedInputStream(new FileInputStream(filePath));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        new Thread(() -> {
            // https://www.kernel.org/doc/Documentation/input/joystick-api.txt

            while (true) {
                byte[] bytes = new byte[24];
                ByteBuffer bb = ByteBuffer.wrap(bytes);
                bb.order(ByteOrder.nativeOrder());
                try {
                    stream.read(bytes, 0, 24);
                } catch (IOException e) {
                    e.printStackTrace();
                }

                bb.rewind();
				long tv_sec = bb.getLong();
				long tv_usec = bb.getLong();
				short type = bb.getShort();
				short code = bb.getShort();
				int value = bb.getInt();

				if(code == 2){
					System.out.println(value);
				}

                if (type == 1) {
                    buttonPresses.put(code, value != 0);
                        if (value == 1) {
                            listeners.forEach(c -> c.accept(buttonIDs.get(code)));
                        }
                } else if (type == 3){
                    analogValues.put(code, value);
                }
            }

        }).start();
    }

    public void init(){
    	for(Short s : buttonNames.values()){
    		buttonPresses.put(s, false);
		}
		for(short s : analogNames.values()){
    		analogValues.put(s, (analogMax.get(s) + analogMin.get(s)) / 2);
		}
	}

	public void addListener(Consumer<String> listener) {
		listeners.add(listener);
	}

    public int getAnalogValue(String name){
    	short id = analogNames.get(name);
    	return getAnalogValue(id);
	}

	public int getAnalogValue(int id){
    	return analogValues.get((short)id);
	}

	public int getAnalogMin(String name){
		short id = analogNames.get(name);
		return analogMin.get(id);
	}

	public int getAnalogMax(String name){
		short id = analogNames.get(name);
		return analogMax.get(id);
	}

	public boolean getButtonValue(String name){
    	short id = buttonNames.get(name);
    	return getButtonValue(id);

	}

	public float getNormalisedAnalogValue(String name) {
	    float val = getAnalogValue(name);
		float min = getAnalogMin(name);
		float max = getAnalogMax(name);
		float t = 2*((val - min)/(max - min)) - 1;
		if (0.01 > Math.abs(t)) {
			t = 0;
		}
		return t;
	}

	public boolean getButtonValue(int id){
		return buttonPresses.get((short)id);
	}

	protected void setButtonName(String name, int id){
		buttonNames.put(name, (short)id);
		buttonIDs.put((short)id, name);
	}

	protected void setAnalogName(String name, int id, int min, int max){
		analogNames.put(name, (short)id);
		analogIDs.put((short)id, name);
		analogMin.put((short)id, min);
		analogMax.put((short)id, max);
	}

	public static Gamepad createGamepad(String filePath, int controllerType){
		switch(controllerType){
			case 0:
				return new Controller(filePath);
			case 1:
				return new DualShock4(filePath);
			case 2:
				return new JoyStick(filePath);
			case 3:
				return new FLY5Stick(filePath);
			case 4:
				return new BalanceBoard(filePath);
		}
		throw new IllegalArgumentException("Invalid controller type");
	}

}
