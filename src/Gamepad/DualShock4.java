package Gamepad;

public class DualShock4 extends Controller{
	public DualShock4(String filePath) {
		super(filePath);
		setButtonName("X", 308);
		setButtonName("Y", 307);
		setButtonName("TOUCHPAD", 316);
		setButtonName("L_TRIGGER", 312);
		setButtonName("R_TRIGGER", 313);

		setAnalogName("L_STICK_X", 0, 0, 255);
		setAnalogName("L_STICK_Y", 1, 0, 255);
		setAnalogName("R_STICK_X", 3, 0,255);
		setAnalogName("R_STICK_Y", 4, 0, 255);
	}
}
