import processing.core.PImage;

/**
 * Stores the position and direction of Maze Walls.
 */
public class MazeWall {
    int x,y;
    PImage wallImage;

    public MazeWall(int x, int y, boolean vertical) {
        this.x = x;
        this.y = y;
        this.vertical = vertical;
    }

    boolean vertical;
}
