from sys import argv
from struct import unpack
import ctypes

print(ctypes.sizeof(ctypes.c_long))

with open(argv[1], "rb") as js:
    while True:
        chunk = js.read(24)
        (time, tv_usec, typ, code, value) = unpack("QlHHi", chunk)
        if typ == 0 or code == 0 or code == 1 or code == 3 or code == 4:
            continue
        print("time: {}".format(time))
        print("time2: {}".format(tv_usec))
        print("type: {}".format(typ))
        print("code: {}".format(code))
        print("value: {}".format(value))
