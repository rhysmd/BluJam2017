#ifdef GL_ES
precision mediump float;
precision mediump int;
#endif

#define PROCESSING_TEXTURE_SHADER

varying vec4 vertTexCoord;
uniform sampler2D texture;

void main(void)
{
  	vec2 p = vertTexCoord.st;
  	vec4 rgba = texture2D(texture, p);

  	if (rgba.r == rgba.b) {
        gl_FragColor = vec4((1.0/21.0), (1.0/254.0), 0.0, 0.0);
    } else {
        gl_FragColor = rgba;
    }
}
